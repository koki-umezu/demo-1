$(function() {
	//確認ダイヤログを表示する処理

	$('.delete, .message, .comment').on('click', function() {
		let messageLog;
		switch($(this).attr('class')){
		case 'delete':
			messageLog ='つぶやきを削除してもよろしいですか？';
			break;
		case 'message':
			let message = $('.send-message input[name="content"]').val()
			if(!message){
				messageLog = '文章を入力してください';
				alert(messageLog);
				return false;
			}
			messageLog ='"' + $('.send-message input[name="content"]').val() + '"' + 'の内容で投稿してよろしいですか？';
			break;
		}

		let result = confirm(messageLog);

		if(result){
			return true;
		}else{
			return false;
		}
	});

});