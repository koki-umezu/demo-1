package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.logic.Logic;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	@Autowired
	Logic logic;

	//画面表示領域
	// 投稿内容表示画面(TOP画面)
	@GetMapping
	public ModelAndView top(String startDate, String endDate) {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = new ArrayList<>();
		if (startDate == null && endDate == null) {
			contentData = reportService.findAllReport();
		} else {
			contentData = reportService.findByDate(startDate, endDate);
		}
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		List<String> errorMessage = new ArrayList<String>();
		mav.addObject("errorMessage", errorMessage);
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//投稿編集画面
	@GetMapping("/edit")
	public ModelAndView editContent(int contentId) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessage = new ArrayList<String>();
		Optional<Report> contentData = reportService.findReport(contentId);
		if (!contentData.isPresent()) {
			mav = logic.notFoundID(mav,errorMessage);
			return mav;
		}
		mav.setViewName("/edit");
		mav.addObject("errorMessage", errorMessage);
		mav.addObject("formModel", contentData.get());
		return mav;
	}

	// コメント投稿画面
	@GetMapping("/comment/{id}")
	public ModelAndView commentContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessage = new ArrayList<String>();
		// form用の空のentityを準備
		Comment comment = new Comment();
		// 画面遷移先を指定
		mav.setViewName("/comment");
		comment.setMessageId(id);
		Optional<Report> contentData = reportService.findReport(id);
		if (!contentData.isPresent()) {
			mav = logic.notFoundID(mav,errorMessage);
			return mav;
		}
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		return mav;
	}

	//コメント編集画面
	@GetMapping("/comment/edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessage = new ArrayList<String>();
		Optional<Comment> commentData = commentService.findComment(id);
		if (!commentData.isPresent()) {
			mav = logic.notFoundID(mav,errorMessage);
			return mav;
		}
		mav.setViewName("/edit_comment");
		mav.addObject("formModel", commentData.get());
		return mav;
	}

	// 処理領域
	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント投稿処理
	@PostMapping("/comment/add")
	public ModelAndView addCommentContent(@ModelAttribute("formModel") Comment comment) {
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿削除処理
	@PostMapping("/delete")
	public ModelAndView deleteContent(int contentId) {
		reportService.deleteReport(contentId);
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	// 投稿編集処理
	@PostMapping("/edit")
	public ModelAndView editContent(@ModelAttribute("formModel") Report report) {
		reportService.editReport(report);
		return new ModelAndView("redirect:/");
	}

	// コメント編集処理
	@PutMapping("/comment/edit/{id}")
	public ModelAndView editcomment(@ModelAttribute("formModel") Comment comment) {
		commentService.editComment(comment);
		return new ModelAndView("redirect:/");
	}

}