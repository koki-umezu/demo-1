package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Modifying
	@Query("update Report u set updatedDate =  CURRENT_TIMESTAMP where id = :id")
	public int updateTimestamp(@Param("id") Integer id);

	@Query("select u from Report u where u.createdDate between :startDate and :endDate order by u.updatedDate desc")
	public List<Report> findByDate(Timestamp startDate, Timestamp endDate);
}