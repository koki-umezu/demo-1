package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
	@Modifying
	@Query("update Comment u set updatedDate =  CURRENT_TIMESTAMP where id = :id")
	public int updateTimestamp(@Param("id") Integer id);

	@Query("select u from Comment u where u.messageId = id")
	public Optional<Comment> commentFindByMessageId(@Param("messageId") Integer id);

}
