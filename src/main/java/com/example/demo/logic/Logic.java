package com.example.demo.logic;

import java.util.List;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class Logic {

	public ModelAndView notFoundID(ModelAndView mav, List<String> errorMessage){

		errorMessage.add("パラメータが不正です。");
		mav.addObject("errorMessage", errorMessage);
		mav.setViewName("/top");
		return mav;

	}


}
