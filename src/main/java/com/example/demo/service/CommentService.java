package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	@Autowired
	ReportRepository reportRepository;

	//レコード全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	// 指定レコード取得
	public Optional<Comment> findComment(int id) {
		return commentRepository.findById(id);
	}

	//指定レコード取得(メッセージIDから)
	public Optional<Comment> commentFindByMessageId(int id) {
		return commentRepository.commentFindByMessageId(id);
	}

	// レコード登録
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
		reportRepository.updateTimestamp(comment.getMessageId());
	}

	// レコード削除
	public void deleteComment(int id) {
		commentRepository.deleteById(id);
	}

	// レコード編集
	public void editComment(Comment comment) {
		commentRepository.save(comment);
		commentRepository.updateTimestamp(comment.getId());
		reportRepository.updateTimestamp(comment.getMessageId());
	}

}
