package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	public List<Report> findByDate(String start, String end) {

		Timestamp startDate = new Timestamp(0);
		Timestamp endDate = new Timestamp(0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date tmpStartDate;

		try {
			if (!start.isEmpty()) {
				start = start + " 00:00:00";
				tmpStartDate = sdf.parse(start);
			} else {
				tmpStartDate = sdf.parse("2020-01-01 00:00:00");
			}
			Long longStartDate = tmpStartDate.getTime();
			startDate.setTime(longStartDate);

			if (!end.isEmpty()) {
				end = end + " 23:59:99";
				Date tmpEndDate = sdf.parse(end);
				Long longEndDate = tmpEndDate.getTime();
				endDate.setTime(longEndDate);
			} else {
				endDate.setTime(System.currentTimeMillis());
			}

		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return reportRepository.findByDate(startDate, endDate);
	}

	// 指定レコード取得
	public Optional<Report> findReport(int id) {
		return reportRepository.findById(id);
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(int id) {
		reportRepository.deleteById(id);
	}

	// レコード編集
	public void editReport(Report report) {
		reportRepository.save(report);
		reportRepository.updateTimestamp(report.getId());
	}
}